{{--
  Template Name: Tema Casting
--}}

@extends('layouts.app')

@section('content')


  @isfield('activar_bg', true)
    @component('components.bg-image',
      [
          'image' => $background_img_casting['ID'],
          'class' => 'casting-bg',
      ])
    @endcomponent
  @endfield

  @isfield('activar_bg', false)
    @component('components.bg-image',
      [
          'image' => $background_img_casting['ID'],
          'class' => 'casting-bg',
      ])
    @endcomponent
  @endfield

  @while(have_posts()) @php the_post() @endphp

  <div class="container">
    <div class="row">
      <div class="col-12"> @include('partials.page-header')</div>
    </div>
    <div class="row altoCompleto">
      <div class="col-12" style="padding-top: 15%;">
        @include('partials.content-page')
      </div>
    </div>
  </div>
  @endwhile
@endsection
