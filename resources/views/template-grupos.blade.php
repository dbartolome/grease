{{--
  Template Name: Tema Grupos
--}}

@extends('layouts.app')

@section('content')


  @isfield('activar_bg', true)
    @component('components.bg-image',
      [
          'image' => $background_img_grupos['ID'],
          'class' => 'grupos-bg',
      ])
    @endcomponent
  @endfield

  @isfield('activar_bg', false)
    @component('components.bg-image',
      [
          'image' => $bg_default_grupos['ID'],
          'class' => 'home-bg',
      ])
    @endcomponent
  @endfield
  <?php
  $i = 0;
  ?>

  @while(have_posts()) @php the_post() @endphp


  <div class="container">
    <div class="row">
      <div class="col-12"> @include('partials.page-header')</div>
      <div class="col-12 col-md-4 col-lg-3 order-1 order-md-0">
        @hasfield('grupos_img', 'url', 1)
          <img class="grupos-img d-block" src="@field('grupos_img', 'url', 1)" alt="@field('grupos_img', 'alt', 1)" />
        @endfield
      </div>
      <div class="bloque col-12 col-md-8 col-lg-7 order-0 my-2 align-content-end">
        @fields('grupos_bloques')
        <div class="capa<?php echo $i; ?>">
          <h2 class="text-uppercase my-3" style="color: @sub('grupos_color')">@sub('grupos_h2')</h2>
          @sub('grupos_contenido')
          <?php $i++; ?>
        </div>
        @endfields

      </div>
      <div class="col-12">
        @include('partials.content-page')
      </div>
    </div>
  </div>

  @endwhile
@endsection

<style>
  .capa0 {
    background-color: rgba(0,0,0,0.5);
    padding: 4%;
    margin-bottom: 5%;
  }

  .capa1 {
    background-color: rgba(0,0,0,0.5);
    padding: 4%;
  }
</style>
