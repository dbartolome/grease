{{--
  Template Name: Tema Entradas
--}}

@extends('layouts.app')

@section('content')


@isfield('activar_bg', true)
  @component('components.bg-image',
  [
    'image' => $background_img_entradas['ID'],
    'class' => 'entradas-bg',
  ])
  @endcomponent
@endfield

@isfield('activar_bg', false)
  @component('components.bg-image',
  [
    'image' => $bg_default_entradas['ID'],
    'class' => 'home-bg',
  ])
  @endcomponent
@endfield

@while(have_posts()) @php the_post() @endphp

<div class="container">
  <div class="row">
    <div class="col-12 order-1"> @include('partials.page-header')</div>
  </div>
</div>

<!-- <section class="sec-superior">
  <div class="container">
    <div class="row">

      @field('shortCode')
    </div>
    <div class="row">
      <div class="col-12"><p style="text-align: center; font-size: 0.7em"> *El calendario de entradas actualmente a la venta no significa el final de la temporada.</p></div>
    </div>
  </div>
</section>

<section class="sec-compra">
  <div class="container">
    <div class="row">
      <div class="col-12">Seccion compra aquí</div>
    </div>
  </div>
</section>
-->
<section class=" sec-venta">
  @include('partials.entradas-venta')
</section>

<section class="sec-info">
  @include('partials.entradas-info')
</section>

<section class="sec-promo">
  @include('partials.entradas-promo')
</section>

<section class="sec-ubicacion">
  @include('partials.entradas-ubica')
</section>

<section class="sec-puntosventa pt-5 pb-2">
  @include('partials.entradas-otros-puntos')
</section>

<section class="sec-page py-5">
  <div class="container">
    <div class="row">
      <div class="col-12">
        @include('partials.content-page')
      </div>
    </div>
  </div>
</section>

</div>

@endwhile
@endsection
