{{--
  Template Name: Tema Sinopsis
--}}

@extends('layouts.app')

@section('content')

  @isfield('activar_bg', true)
    @component('components.bg-image',
      [
          'image' => $background_img_sinopsis['ID'],
          'class' => 'sinopsis-bg',
      ])
    @endcomponent
  @endfield

  @isfield('activar_bg', false)
    @component('components.bg-image',
      [
          'image' => $bg_default_sinopsis['ID'],
          'class' => 'home-bg',
      ])
    @endcomponent
  @endfield

  @php
  $sinopsis_page = get_page_by_path('sinopsis', 'ARRAY_N');
  // var_dump($sinopsis_page[0]);
  @endphp


  @while(have_posts()) @php the_post() @endphp

<div class="container-fluid" style="background-image: url(/wp-content/uploads/2020/01/Edificios.png); background-size: contain; background-repeat: no-repeat; background-position: bottom">
  <div class="container">
    <div class="row">
      <div class="col-12"> @include('partials.page-header')</div>
    </div>
    <div class="row">

      <div class="col-12 mb-2 contTexto">GREASE EL MUSICAL es el fenómeno de la cultura pop que narra la vida en el instituto de una pequeña ciudad americana en los años 50, habiéndose convertido en el gran musical del Rock and Roll por excelencia, además de en una de las películas musicales más taquillera de la historia.<br /><br />
        Celebrando en 2021 los 50 años desde su estreno original en Chicago, GREASE continúa siendo una de las principales fuentes de inspiración para nuevas creaciones artísticas. SOM Produce se suma a este aniversario y estrena una nueva producción con una ambiciosa e innovadora puesta en escena, a cargo del mismo equipo creativo de Billy Elliot.</div>
      <div class="col-12 order-3 col-lg-6 order-lg-2 align-self-end">
        {!! $img_sinopsis !!}
      </div>
      <div class="col-12 order-2 col-lg-6 order-lg-3 align-self-center contTexto">
      @include('partials.content-page')
      </div>
    </div>
  </div>
</div>
  @endwhile
@endsection
