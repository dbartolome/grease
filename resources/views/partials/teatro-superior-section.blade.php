<section class="superior">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-6 d-flex flex-column justify-content-center">


      <div class="row informacion">
        <div class="col-12">
          <p class="direccion text-uppercase font-weight-bold mb-0">
            @field('teatro_dir')
          </p>
          <p class="poblacion">
            @field('teatro_poblacion')
          </p>
        </div>
        <div class="col-12 btnVermapa">
          <a class="vermapa" href="@field('enlace_mapa')" target="_blank">Ver Mapa</a>
        </div>
        <div class="col-12">
          <ul>
            @fields('teatro_telefonos')
            <li>
              @sub('teatro_tel_label') @sub('teatro_tel_numero')
            </li>
            @endfields
          </ul>
        </div>
        <div class="h-funciones col-12">
          <h3>Horarios de funciones</h3>
          <ul>
            @fields('teatro_horarios')
              <li>
                @sub('teatro_horario')
              </li>
            @endfields
          </ul>
        </div>
        <div class="h-taquilla col-12">
          <h3>Horarios de taquilla</h3>
          <ul>
            @fields('teatro_horarios_taquilla')
              <li>
                @sub('teatro_horario_taquilla')
              </li>
            @endfields
          </ul>
        </div>
        <div class="col-12">
          <p class="accesible text-right text-uppercase font-weight-bold">
            Teatro accesible
          </p>
        </div>
      </div>


      </div>
      <div class="col-12 col-sm-6">
        @thumbnail('large')
      </div>
    </div>
  </div>
</section>
