<footer class="content-info">

  @hasoption('entradas_footer')
  <div class="entradas-wrap d-lg-none">
    <div class="entradas">
      <a href="@option('entradas_link_footer')">
        <img src="@option('entradas_footer', 'url')" alt="@option('entradas_footer', 'alt')" />
      </a>
    </div>
  </div>
  @endoption

  <div class="container">
    <div class="row">
      @hasoption('texto_footer')
      <div class="textofooter col-12 col-lg-7 order-3 order-lg-1 align-self-lg-center text-center text-lg-left">
        @option('texto_footer')
      </div>
      @endoption

      @hasoption('social')
      <div class="col-12 col-lg-3 order-1 order-lg-2 align-self-lg-center">
        <ul class="col-12 social text-center">
          @options('social')
          <li class="d-inline-block">
            <a class="text-white d-block p-2" href="@sub('social_url')" target="_blank">
              @hassub('social_icon')
             <!-- <span class="icon">
                <i class="fab fa-@sub('social_icon')"></i>

              </span> -->
              <img src="/wp-content/themes/grease/dist/images/@sub('social_icon').png" style="width: 1.3em">
              @endsub
            </a>
          </li>
          @endoptions
        </ul>
      </div>
      @endoption

      <div class="som-produce col-12 col-lg-2 order-2 order-lg-3 align-self-lg-center">
        <img src="@asset("images/som-produce.png")" alt="Logotipo Som Produce" />
      </div>
    </div>

  </div>
</footer>
