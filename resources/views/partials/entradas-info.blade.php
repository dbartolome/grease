<div class="container py-5">
  <div class="row justify-content-center">

    <div class="col-12 text-center mb-5">
      <h2>@field('entradas_info_titulo_sec')</h2>
    </div>

        <ul class="listado d-sm-none">
          @if (have_rows('entradas_info_franjas_horarias'))
          @while (have_rows('entradas_info_franjas_horarias'))
          @php the_row(); @endphp

          <li class="bloque-franja">
            <h3>@sub('entradas_info_franja_epigrafe')</h3>
            <ul>
              <li class="d-flex">
                <div class="ubicacion">Platea preferente</div>
                <div class="precio">@sub('entradas_info_franja_precio_platea_pref') €</div>
              </li>

              <li class="d-flex">
                <div class="ubicacion">Platea</div>
                <div class="precio">@sub('entradas_info_franja_precio_platea') €</div>
              </li>

              <li class="d-flex">
                <div class="ubicacion">Palcos</div>
                <div class="precio">@sub('entradas_info_franja_precio_palcos') €</div>
              </li>

              <li class="d-flex">
                <div class="ubicacion">Anfiteatro 1</div>
                <div class="precio">@sub('entradas_info_franja_precio_anfi1') €</div>
              </li>

              <li class="d-flex">
                <div class="ubicacion">Anfiteatro 2, (Filas 1 y 2)</div>
                <div class="precio">@sub('entradas_info_franja_precio_anfi2_A') €</div>
              </li>

              <li class="d-flex">
                <div class="ubicacion">Anfiteatro 2, (Fila 3)</div>
                <div class="precio">@sub('entradas_info_franja_precio_anfi2_B') €</div>
              </li>
            </ul>
          </li>
        @endwhile
        @endif
      </ul>





    <div class="d-none d-sm-block tabla-col col-lg-7 text-center mb-5">
      <ul class="tabla">
        <li class="franja">
          <ul>
            <li class="epi" style="background-color: #1b0317d7;">
              Funciones
            </li>
            <li class="platea-pref" style="background-color: #1b0317d7;">
              Platea preferente
            </li>
            <li class="platea" style="background-color: #1b0317d7;">
              Platea
            </li>
            <li class="palcos" style="background-color: #1b0317d7;">
              Palcos
            </li>
            <li class="anfi1" style="background-color: #1b0317d7;">
              Anfiteatro 1
            </li>
            <li class="anfi2 anfi2A" style="background-color: #1b0317d7;">
              Anfiteatro 2, (Filas 1 y 2)
            </li>
            <li class="anfi2 anfi2B" style="background-color: #1b0317d7;">
              Anfiteatro 2, (Fila 3)
            </li>
          </ul>
        </li>

        @if (have_rows('entradas_info_franjas_horarias'))
        @while (have_rows('entradas_info_franjas_horarias'))
        @php the_row(); @endphp
        <li class="franja">
          <ul>
            <li class="epi">
              @sub('entradas_info_franja_epigrafe')
            </li>
            <li class="precio platea-pref">
              @sub('entradas_info_franja_precio_platea_pref') €
            </li>
            <li class="precio platea">
              @sub('entradas_info_franja_precio_platea') €
            </li>
            <li class="precio palcos">
              @sub('entradas_info_franja_precio_palcos') €
            </li>
            <li class="precio anfi1">
              @sub('entradas_info_franja_precio_anfi1') €
            </li>
            <li class="precio anfi2 anfi2A">
              @sub('entradas_info_franja_precio_anfi2_A') €
            </li>
            <li class="precio anfi2 anfi2B">
              @sub('entradas_info_franja_precio_anfi2_B') €
            </li>
          </ul>
        </li>
        @endwhile
        @endif

      </ul>
    </div>

    <div class="teatro col-lg-5">
      @svg('teatro')
    </div>

  </div>
</div>

<div class="especiales-wrap py-4">
  <div class="container">
    <div class="row justify-content-center">
<div class="col-12 mb-5">
  <h2 style="text-align: center; color: #e3609e; font-size: 1.6rem; font-weight: bold; line-height: 1.2; margin-bottom: 0;"> CONDICIONES EXCLUSIVAS DE PREVENTA:</h2>
  <div style="text-align: center;">Solo por tiempo limitado</div>
</div>
      <div class="especiales col-12">
        @if (have_rows('entradas_info_especiales'))
        @while (have_rows('entradas_info_especiales'))
        @php the_row(); @endphp
        <div class="especial">
          <header class="titulo col-3"> @sub('entradas_info_especiales_titulo')</header>
          <div class="especiales-cont col-9">
            @sub('entradas_info_especiales_tcontenido')
          </div>
        </div>
        @endwhile
        @endif
      </div>


    </div>
  </div>

  <div class="container">
    <div class="row justify-content-center">

      <div class="notas col-12 py-3 mt-4">
        @if (have_rows('entradas_info_notas'))
        @while (have_rows('entradas_info_notas'))
        @php the_row(); @endphp
        <div class="nota">
          @sub('entradas_info_nota')
        </div>
        @endwhile
        @endif
      </div>


    </div>
  </div>

</div>
