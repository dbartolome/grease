<div class="container py-5">
  <div class="row justify-content-center">

    <div class="col-12 text-center">
      <h2>@field('entradas_promo_titulo_sec')</h2>
    </div>


    @if (have_rows('entradas_promo_cards'))
      @while (have_rows('entradas_promo_cards'))
        @php the_row(); @endphp

        @php $img_card = get_sub_field('entradas_promo_titulo_logo_card'); @endphp

        <div class="venta-card col-12 col-sm-6 col-lg-4 text-center p-3">

          <h3 class="venta-card-header">
            @if ($img_card)
              <img src="@sub('entradas_promo_titulo_logo_card', 'sizes', 'medium')"
              alt="@sub('entradas_promo_titulo_logo_card', 'alt')" />
            @else
              @sub('entradas_promo_titulo_card')
            @endif
          </h3>

          @if (have_rows('entradas_promo_card_contents'))
          @while (have_rows('entradas_promo_card_contents'))

            @php
              the_row();
              $layout = get_row_layout();
            @endphp

            @if ($layout == 'entradas_promo_layout_enlace')
              @php $tipo = get_sub_field('entradas_promo_layout_enlace_tipo'); @endphp

              @if ($tipo == 'img')
                <a class="link-img d-block" href="@sub('entradas_promo_layout_enlace_enlace', 'url')"
                  target="@sub('entradas_promo_layout_enlace_enlace', 'target')" alt="@sub('entradas_promo_layout_enlace_enlace', 'alt')">
                  <img src="@sub('entradas_promo_layout_enlace_imagen', 'sizes', 'medium')"
                    alt="@sub('entradas_promo_layout_enlace_imagen', 'alt')" />
                </a>
              @elseif($tipo == 'boton')
                <a class="link-boton d-block" href="@sub('entradas_promo_layout_enlace_enlace', 'url')"
                  target="@sub('entradas_promo_layout_enlace_enlace', 'target')">
                  @sub('entradas_promo_layout_enlace_enlace', 'title')
                </a>
              @elseif($tipo == 'normal')
                <a class="link-normal d-block" href="@sub('entradas_promo_layout_enlace_enlace', 'url')"
                  target="@sub('entradas_promo_layout_enlace_enlace', 'target')">
                  @sub('entradas_promo_layout_enlace_enlace', 'title')
              </a>
              @endif

              @elseif($layout == 'entradas_promo_layout_texto')
                @sub('entradas_promo_layout_texto_textarea')
              @endif

            @endwhile
          @endif
        </div>

      @endwhile
    @endif

  </div>
</div>
