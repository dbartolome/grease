<header class="banner">
  <div class="cont container-fluid l-3 r-3">
    <div class="row justify-content-center">
      <div class="col-3 col-sm-2 contCentrado">
        <button class="hamburger hamburger--spin" type="button">
          <span class="hamburger-box">
            <span class="hamburger-inner"></span>
          </span>
        </button>
      </div>

      <div class="col-9 col-sm-10 col-lg-8">
        <a class="brand efectosLogo" href="{{ home_url('/') }}">
               <img src="/wp-content/themes/grease/dist/images/homeInteriores/logo.png" alt="logo Grease el musical" class="destello">
              <img src="/wp-content/themes/grease/dist/images/homeInteriores/logogreasecapas.png" alt="logo Grease el musical" class="destello destello-01">
              <img src="/wp-content/themes/grease/dist/images/homeInteriores/logogreasecapas1.png" alt="logo Grease el musical"  class="destello destello-02">
              <img src="/wp-content/themes/grease/dist/images/homeInteriores/logogreasecapas2.png" alt="logo Grease el musical" class="destello destello-03">
        </a>
      </div>

      @hasoption('entradas_header')
        <div class="entradas d-none d-lg-block col-lg-2">
          <a href="@option('entradas_link_header')" class="botonEntradasSup">
            <img src="/wp-content/themes/grease/dist/images/btnEntradas/entradas_2.png" alt="@option('entradas_header', 'alt')" class="imagenesFrame" >
            <img src="/wp-content/themes/grease/dist/images/btnEntradas/entradas_sololetras.png" alt="@option('entradas_header', 'alt')" class="imagenesFrame letras">
            <img src="/wp-content/themes/grease/dist/images/btnEntradas/entradas_estrella1.png" alt="@option('entradas_header', 'alt')" class="imagenesFrame estrella">
            <img src="/wp-content/themes/grease/dist/images/btnEntradas/entradas_linea1.png" alt="@option('entradas_header', 'alt')" class="imagenesFrame lineas">
            <img src="/wp-content/themes/grease/dist/images/btnEntradas/entradas_linea2.png" alt="@option('entradas_header', 'alt')" class="imagenesFrame lineas">
            <img src="/wp-content/themes/grease/dist/images/btnEntradas/entradas_linea3.png" alt="@option('entradas_header', 'alt')" class="imagenesFrame lineas">
            <img src="/wp-content/themes/grease/dist/images/btnEntradas/entradas_linea4.png" alt="@option('entradas_header', 'alt')" class="imagenesFrame lineas">
            <img src="/wp-content/themes/grease/dist/images/btnEntradas/entradas_estrella2.png" alt="@option('entradas_header', 'alt')" class="imagenesFrame estrella">
          </a>



        </div>
      @endoption
      <a href="/teatro-seguro" class="btnTeatroSeguro"><img src="/wp-content/themes/grease/dist/images/btnEntradas/teatroseguro.png" alt="Teatro seguro" width="100%"></a>
    </div>
  </div>
  <nav class="nav-primary">
    @if (has_nav_menu('primary_navigation'))
      {!! wp_nav_menu($primarymenu) !!}
    @endif
  </nav>
</header>
