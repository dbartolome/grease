<div class="container py-5">
  <div class="row justify-content-center">

    <div class="col-12 text-center">
      <h2>@field('entradas_venta_titulo_sec')</h2>
    </div>


      @if (have_rows('entradas_venta_cards'))
        @while (have_rows('entradas_venta_cards'))
          @php the_row(); @endphp

          @php $img_card = get_sub_field('entradas_venta_titulo_logo_card'); @endphp

          <div class="venta-card col-12 col-sm-6 col-lg-4 text-center p-3">

          <h3 class="venta-card-header">
            @if ($img_card)
            <img src="@sub('entradas_venta_titulo_logo_card', 'sizes', 'medium')" alt="@sub('entradas_venta_titulo_logo_card', 'alt')" />
            @else
             @sub('entradas_venta_titulo_card')
            @endif
          </h3>

            @if (have_rows('entradas_venta_card_contents'))
              @while (have_rows('entradas_venta_card_contents'))

                @php
                    the_row();
                    $layout = get_row_layout();
                @endphp

                @if ($layout == 'entradas_layout_enlace')
                  @php $tipo = get_sub_field('entradas_layout_enlace_tipo'); @endphp

                  @if ($tipo == 'img')
                    <a class="link-img d-block" href="@sub('entradas_layout_enlace_enlace', 'url')" target="@sub('entradas_layout_enlace_enlace', 'target')" alt="@sub('entradas_layout_enlace_enlace', 'alt')">
                      <img src="@sub('entradas_layout_enlace_imagen', 'sizes', 'medium')" alt="@sub('entradas_layout_enlace_imagen', 'alt')" />
                    </a>
                  @elseif($tipo == 'boton')
                    <a class="link-boton d-block" href="@sub('entradas_layout_enlace_enlace', 'url')" target="@sub('entradas_layout_enlace_enlace', 'target')">
                      @sub('entradas_layout_enlace_enlace', 'title')
                    </a>
                  @elseif($tipo == 'normal')
                    <a class="link-normal d-block" href="@sub('entradas_layout_enlace_enlace', 'url')" target="@sub('entradas_layout_enlace_enlace', 'target')">
                      @sub('entradas_layout_enlace_enlace', 'title')
                    </a>
                  @endif

                @elseif($layout == 'entradas_layout_texto')
                    @sub('entradas_layout_texto_textarea')
                @endif

              @endwhile
            @endif
          </div>

        @endwhile
      @endif

      <div class="venta-footer col-sm-7 col-lg-5 text-center d-flex flex-column mt-5">
       <!-- <div class="footer-icon w-100">
          <i class="fas fa-users"></i>
        </div> -->
        <div class="footer-texto">
          @field('entradas_venta_footer')
        </div>

      </div>

  </div>
</div>
