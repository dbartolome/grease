<div class="ubica-wraper">
  <div class="container">
    <div class="row justify-content-center">

      <div class="col-12 text-center">
        <h2>Nuestra ubicación</h2>
      </div>

      <div class="col-12 col-lg-6">
        @field('mapa')
      </div>

      <div class="indicaciones col-12 col-lg-6">
        @fields('comollegar')
        <div class="row">
          <div class="icono col-2 offset-1" style="color: @sub('color_icon')">
            @hassub('comollegar_icon')
              <i class="fas fa-@sub('comollegar_icon')"></i>
            @endsub
          </div>
          <div class="col-9">
            @hassub('comollegar_label')
              <p class="w-100 mb-0 font-weight-bold text-uppercase" style="color: @sub('color_icon')">
                @sub('comollegar_label')
              </p>
              @endsub
            @hassub('comollegar_texto')
            <p class="w-100">
              @sub('comollegar_texto')
            </p>
            @endsub
          </div>
        </div>
        @endfields
      </div>

    </div>
  </div>
</div>
