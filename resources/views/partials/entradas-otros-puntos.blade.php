<div class="container">
  <div class="row justify-content-center">

    <div class="col-12 text-center">
      <h2>Otros puntos de venta</h2>
    </div>


      @if (have_rows('entradas_otros_puntos'))
        @while (have_rows('entradas_otros_puntos')) @php the_row(); @endphp

        <div class="col-12 col-sm-6 col-lg-4 py-4 text-center">
          <a href="@sub('entradas_otros_puntos_enlace', 'url')" target="@sub('entradas_otros_puntos_enlace', 'target')">
            @if (get_sub_field('entradas_otros_puntos_img'))
              <img src="@sub('entradas_otros_puntos_img', 'url')" alt="@sub('entradas_otros_puntos_img', 'alt')" />
            @else
              @sub('entradas_otros_puntos_img', 'title')
            @endif
          </a>
        </div>

        @endwhile
      @endif


  </div>
</div>
