@extends('layouts.app')
@section('content')
@component('components.bg-image',
    [
        'image' => $background_img_home['ID'],
        'class' => 'home-bg',
    ])
@endcomponent
<div class="cont container-fluid l-3 r-3">
  <div class="row">
    <div class="col-12">
      <div id="scene-row">

        <div class="frames-wrap" style="position: relative">
          <!-- <img src="/wp-content/themes/grease/dist/images/PLAY.png" id="verVideo" class="btnPlay"> -->
          <div id="scene">
            <div class="frames" data-depth="0.15">


              @php $i = 1; @endphp
              @foreach ($frames as $frame)
                <img class="personaje" id="frame{{ $i }}"
                     src="{{ $frame['url'] }}"
                     srcset="{{ wp_get_attachment_image_srcset($frame['ID'], 'large') }}"
                     sizes=""
                     alt=""

                     style="position: absolute"
                >
                @php $i++; @endphp
              @endforeach
              <img class="personaje" src="@option('animacionFondo', 'url')" srcset="{{ wp_get_attachment_image_srcset($frame['ID'], 'large') }}" id="imgPeloAnime">
              <div class="efectos" data-depth="0.15">
                <img src="/wp-content/themes/grease/dist/images/homeLogo/logo.png" alt="Navidad" class="destello background-navidad">
                <img src="/wp-content/themes/grease/dist/images/homeLogo/logogreasecapas.png" class="destello destello-01">
                <img src="/wp-content/themes/grease/dist/images/homeLogo/logogreasecapas1.png" class="destello destello-02">
                <img src="/wp-content/themes/grease/dist/images/homeLogo/logogreasecapas2.png" class="destello destello-03">
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>

  </div>
</div>
<!-- <div class="contVideo" >
  <div id="btnCerrar"><img src="/wp-content/themes/grease/dist/images/cerrar.png" width="100%"></div>
  <video src="/video/GreasesSpotAnimado.mov" controls style="height: 100%; width: 100%" id="videoHome">
    Tu navegador no implementa el elemento <code>video</code>.
  </video>
</div> -->



@endsection
