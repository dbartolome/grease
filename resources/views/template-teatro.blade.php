{{--
  Template Name: Tema Teatro
--}}

@extends('layouts.app')

@section('content')

  @while(have_posts()) @php the_post() @endphp

  <div class="col-12"> @include('partials.page-header')</div>

  @isfield('activar_bg_teatro_sec_sup', true)
    @component('components.bg-image',
      [
          'image' => $background_img_teatro_sup['ID'],
          'class' => 'teatro-bg',
      ])
    @endcomponent

  @endfield

  @isfield('activar_bg_teatro_sec_sup', false)
    @component('components.bg-image',
      [
          'image' => $bg_default_teatro['ID'],
          'class' => 'teatro-bg',
      ])
    @endcomponent
  @endfield

  @include('partials.teatro-superior-section')

  <section class="medio">
    <div class="container">
      <div class="row">

        <div class="col-12 col-lg-6">
          @field('mapa')
        </div>

        <div class="col-12 col-lg-6 align-self-center">
          @fields('comollegar')
          <div class="row">
            <div class="icono col-2 offset-1" style="color: @sub('color_icon')">
              @hassub('comollegar_icon')
                <i class="fas fa-@sub('comollegar_icon')"></i>
              @endsub
            </div>
            <div class="col-9">
              @hassub('comollegar_label')
                <p class="w-100 mb-0 font-weight-bold text-uppercase" style="color: @sub('color_icon')">
                  @sub('comollegar_label')
                </p>
                @endsub
              @hassub('comollegar_texto')
              <p class="w-100">
                @sub('comollegar_texto')
              </p>
              @endsub
            </div>
          </div>
          @endfields
        </div>

      </div>
    </div>
  </section>

    @isfield('activar_bg', true)
      @component('components.bg-image-section',
        [
            'image' => $background_img_teatro['ID'],
            'class' => 'inferior',
        ])
        @include('partials.teatro-inferior-section')
      @endcomponent

    @endfield

    @isfield('activar_bg', false)
      @component('components.bg-image-section',
        [
            'image' => $bg_default_teatro['ID'],
            'class' => 'inferior',
        ])
        @include('partials.teatro-inferior-section')
      @endcomponent
    @endfield


  @endwhile
@endsection
