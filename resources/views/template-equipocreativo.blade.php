{{--
  Template Name: Tema Equipo Creativo
--}}

@extends('layouts.app')

@section('content')


  @isfield('activar_bg', true)
    @component('components.bg-image',
      [
          'image' => $background_img_equipocreativo['ID'],
          'class' => 'equipocreativo-bg',
      ])
    @endcomponent
  @endfield

  @isfield('activar_bg', false)
    @component('components.bg-image',
      [
          'image' => $bg_default_equipocreativo['ID'],
          'class' => 'home-bg',
      ])
    @endcomponent
  @endfield

  @while(have_posts()) @php the_post() @endphp

  <div class="container">
    <div class="row">
      <div class="col-12"> @include('partials.page-header')</div>
      <div class="col-12">
        @include('partials.content-page')
      </div>
    </div>
  </div>
  @endwhile
@endsection
