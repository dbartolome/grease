{{--
Template Name: Tema Generico
--}}

@extends('layouts.app')

@section('content')


  @isfield('activar_bg', true)
    @component('components.bg-image',
      [
        'image' => $background_img_generico['ID'],
        'class' => 'generico-bg',
      ])
    @endcomponent
  @endfield

  @isfield('activar_bg', false)
    @component('components.bg-image',
      [
        'image' => $bg_default_generico['ID'],
        'class' => 'home-bg',
      ])
    @endcomponent
  @endfield

  @while(have_posts()) @php the_post() @endphp
  @include('partials.page-header')
<div class="container">
  <div class="row">
    <div class="col-12">
      <div class="contenidoPage">
        @include('partials.content-page')
      </div>
    </div>
  </div>
</div>
@endwhile
@endsection
