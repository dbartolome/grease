{{--
  Template Name: Tema Tarjeta Regalo
--}}

@extends('layouts.app')

@section('content')

  @isfield('activar_bg', true)
    @component('components.bg-image',
      [
          'image' => $background_img_tarjetaregalo['ID'],
          'class' => 'home-bg',
      ])
    @endcomponent
  @endfield

  @isfield('activar_bg', false)
    @component('components.bg-image',
      [
          'image' => $bg_default_tarjetaregalo['ID'],
          'class' => 'home-bg',
      ])
    @endcomponent
  @endfield

  @while(have_posts()) @php the_post() @endphp

    <div class="container mb-5">
      <div class="row justify-content-center">
        <div class="col-12"> @include('partials.page-header')</div>
        <div class="col-12">
          {!! $img_tarjetaregalo!!}
        </div>
        <div class="page-content text-center col-sm-6">
          @include('partials.content-page')
        </div>
        <div class="importes col-12 text-center">
          @hasfield('importes')
            @fields('importes')
              <a class="d-inline-block px-4 py-2 m-3" href="@sub('url_tarjeta')">@sub('importe')<span>€</span> </a>
            @endfields
          @endfield
        </div>
      </div>
    </div>

    <div class="canjear">
      <div class="container">
        <div class="row">
          <h2 class="d-block text-center w-100 mb-5">Cómo canjear tu tarjeta regalo</h2>
          @hasfield('canjear')
          <ol>
            @fields('canjear')
              <li class="">
                @sub('paso')
              </li>
            @endfields
          </ol>
          @endfield
        </div>
      </div>
    </div>

    <div class="preguntas my-5">
      <div class="container">
        <div class="row">
          <h2 class="d-block text-center w-100 mb-5">Preguntas frecuentes</h2>
        </div>
      </div>
      @hasfield('preguntas')
        @php $counter = 1; @endphp
          <div class="accordion" id="preguntas">
            @fields('preguntas')
            <div class="card mb-3">
              <div class="card-header py-0" id="heading-{{ $counter }}">
                <div class="container">
                  <div class="row">
                    <h3 class="mb-0 d-block w-100">
                      <button class="btn btn-link d-block w-100" type="button" data-toggle="collapse" data-target="#collapse-{{ $counter }}" aria-expanded="true" aria-controls="collapseOne">
                        {{ $counter }}. @sub('pregunta')
                      </button>
                    </h3>
                  </div>
                </div>
              </div>
              <div id="collapse-{{ $counter }}" class="collapse" aria-labelledby="heading-{{ $counter }}" data-parent="#preguntas">
                <div class="card-body">
                  <div class="container">
                    <div class="row justify-content-center">
                      <div class="wrap col-sm-8 col-lg-6">
                        @php $respuesta = get_sub_field('respuesta'); @endphp
                        @wpautop( $respuesta)
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          @php $counter++; @endphp
          @endfields
        </div>
      @endfield
    </div>

  @endwhile

@endsection
