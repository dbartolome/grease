/* eslint-disable no-unused-vars */
import Parallax from 'parallax-js';

export default {
  init() {
    // JavaScript to be fired on the home page

    let scene = document.getElementById('scene');
    let parallaxInstance = new Parallax(scene);


    $('#verVideo').click(function(){
      $('.contVideo').fadeIn('slow',function(){
        $('#videoHome').trigger('play');
      });
    });
    $('#btnCerrar').click(function(){
      $('.contVideo').fadeOut('fast',function(){
        $('#videoHome').trigger('pause');
      });
    });


    function recolocarFoto() {
      var ventana_ancho = $(window).width();
      var ventana_alto = $(window).height();
      if(ventana_ancho <= 768) {
        $('#miImagen').attr('src','/wp-content/themes/grease/dist/images/estreno_20Octubre.png');
      } else if (ventana_ancho >= 768) {
        $('#miImagen').attr('src','/wp-content/themes/grease/dist/images/estreno_20OctubreV.png');
      }
    }

    $(document).ready(function($){
     // recolocarFoto();
    });

    $(window).resize(function(){
    //  recolocarFoto();
    })

    function cargarPelo() {
      $('#frame1').animate({'opacity': 1}, 500);
      $('#frame2').animate({'opacity': 1}, 500);
      $('#frame3').animate({'opacity': 1}, 500, function(){
        ocultarPelo();
      });

    }
    function ocultarPelo() {
      $('#frame3').animate({'opacity': 0}, 500);
      $('#frame2').animate({'opacity': 0.3}, 500, function(){
          cargarPelo();
      });

    }

    cargarPelo();

  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};


