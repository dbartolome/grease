export default {
  init() {
    // JavaScript to be fired on all pages

    // hamburger
    $('.hamburger').click(function () {
      $(this).toggleClass('is-active');
      $('#menu-principal').toggleClass('abierto');
    });

    $(document).on('scroll', function(){
      var scroll = $(window).scrollTop();
      console.log(scroll);
      if(scroll > 30) {
        $('.banner').addClass('sticky');
      } else if (scroll < 30) {
        $('.banner').removeClass('sticky');
      }
    });


  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
