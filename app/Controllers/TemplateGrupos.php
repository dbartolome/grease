<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class TemplateGrupos extends Controller
{
    public function backgroundImgGrupos()
    {
        return get_field('fondo_img');
    }

    public function bgDefaultGrupos()
    {
        return get_field('background_img_home', 'option');
    }

}
