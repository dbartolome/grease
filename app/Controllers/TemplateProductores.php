<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class TemplateProductores extends Controller
{
    public function backgroundImgProductores()
    {
        return get_field('fondo_img');
    }

    public function bgDefaultProductores()
    {
        return get_field('background_img_home', 'option');
    }
}

