<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class TemplateEntradas extends Controller
{
    public function backgroundImgEntradas()
    {
        return get_field('fondo_img');
    }

    public function bgDefaultEntradas()
    {
        return get_field('background_img_home', 'option');
    }

    public function deTeatro()
    {
        $teatro_page = get_page_by_path('teatro', 'ARRAY_N');

        return [
            'id' => $teatro_page[0],
            'dir' => get_field('teatro_dir', $teatro_page[0]),
            'pob' => get_field('teatro_poblacion', $teatro_page[0]),
            'map' => get_field('mapa', $teatro_page[0]),
        ];

    }

}
