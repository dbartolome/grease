<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class TemplateTeatro extends Controller
{
    public function backgroundImgTeatro()
    {
        return get_field('fondo_img');
    }

    public function bgDefaultTeatro()
    {
        return get_field('background_img_home', 'option');
    }

    public function backgroundImgTeatroSup()
    {
        return get_field('fondo_teatro_img');
    }
}
