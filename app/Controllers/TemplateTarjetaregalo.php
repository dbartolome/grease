<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class TemplateTarjetaregalo extends Controller
{
    public function backgroundImgTarjetaregalo()
    {
        return get_field('fondo_img');
    }

    public function bgDefaultTarjetaregalo()
    {
        return get_field('background_img_home', 'option');
    }

    public function imgTarjetaregalo()
    {
        $output = ' ';
        $image = get_field('img_tarjetaregalo');
        $size = '100%'; // (thumbnail, medium, large, full or custom size)
        if( $image ) {

             $output = '<img src="' .$image['url'].'" alt=" ' . $image['title'] . ' " width=" '. $size .' ">';
        }

        return $output;
    }
}

