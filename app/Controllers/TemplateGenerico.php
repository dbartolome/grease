<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class TemplateGenerico extends Controller
{
    public function backgroundImgGenerico()
    {
        return get_field('fondo_img');
    }

    public function bgDefaultGenerico()
    {
        return get_field('background_img_home', 'option');
    }

}
