<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class FrontPage extends Controller
{
    public function backgroundImgHome()
    {
        return get_field('background_img_home', 'option');
    }

    public function frames()
    {
        $frames = [];
        if( have_rows('frames_foreground_img', 'options') ):
            while ( have_rows('frames_foreground_img', 'options') ) : the_row();
                $frames[] = get_sub_field('foreground_img_home');
            endwhile;
        endif;

        return $frames;
    }

    public function backgroundAnima()
    {
        return get_field('animacionFondo', 'option');
    }


}
