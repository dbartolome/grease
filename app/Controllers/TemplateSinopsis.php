<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class TemplateSinopsis extends Controller
{
    public function backgroundImgSinopsis()
    {
        return get_field('fondo_img');
    }

    public function bgDefaultSinopsis()
    {
        return get_field('background_img_home', 'option');
    }

    public function imgSinopsis()
    {
        $output = ' ';
        $image = get_field('img_sinopsis');
        $size = '100%'; // (thumbnail, medium, large, full or custom size)
        if( $image ) {

             $output = '<img src="' .$image['url'].'" alt=" ' . $image['title'] . ' " width=" '. $size .' ">';
        }

        return $output;
    }
}

