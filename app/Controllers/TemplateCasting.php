<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class TemplateCasting extends Controller
{
    public function backgroundImgCasting()
    {
        return get_field('fondo_img');
    }

    public function bgDefaultCasting()
    {
        return get_field('background_img_home', 'option');
    }

}
