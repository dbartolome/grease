<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class TemplateEquipocreativo extends Controller
{
    public function backgroundImgEquipocreativo()
    {
        return get_field('fondo_img');
    }

    public function bgDefaultEquipocreativo()
    {
        return get_field('background_img_home', 'option');
    }

}
