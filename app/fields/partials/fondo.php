<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$fondo = new FieldsBuilder('fondo');

$fondo
    ->addTab('fondo', ['placement' => 'left'])
        ->addImage('fondo_img', [
            'label' => 'Imagen de fondo personalizado para esta página',
            'instructions' => 'Imagen para el fondo de la página',
            'required' => 0,
            'return_format' => 'array',
            'preview_size' => 'medium',
        ])
        ->addTrueFalse('activar_bg', [
            'label' => 'Activar este fondo',
            'instructions' => 'El fondo elegido aquí sólo aparecerá si está activo. En caso contrario, aparecerá el fondo por defecto (que es el que se defina para portada en las opciones del tema).',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'message' => '',
            'default_value' => 0,
            'ui' => 1,
            'ui_on_text' => '',
            'ui_off_text' => '',
        ]);

return $fondo;
