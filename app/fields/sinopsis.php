<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$sinopsis_page = get_page_by_path('sinopsis', 'ARRAY_N');

$sinopsis = new FieldsBuilder('sinopsis');

$sinopsis
    ->setLocation('post_type', '==', 'page')
    ->and('post', '==', $sinopsis_page[0]);

$sinopsis
    ->addFields(get_field_partial('partials.fondo'))
    ->addTab('Imagen', ['placement' => 'left'])
    ->addImage('img_sinopsis', [
        'label' => 'Imagen para la pagina de sinopsis',
        'instructions' => 'Imagen para el contenido de sinopsis',
        'required' => 0,
        'return_format' => 'array',
        'preview_size' => 'medium',
        'library' => 'all',
        'min_width' => '',
        'min_height' => '',
        'min_size' => '',
        'max_width' => '',
        'max_height' => '',
        'max_size' => '',
        'mime_types' => '',
    ])
    ->addImage('img_superior', [
        'label' => 'Imagen para parte superior',
        'instructions' => 'Imagen para el contenido de sinopsis',
        'required' => 0,
        'return_format' => 'array',
        'preview_size' => 'medium',
        'library' => 'all',
        'min_width' => '',
        'min_height' => '',
        'min_size' => '',
        'max_width' => '',
        'max_height' => '',
        'max_size' => '',
        'mime_types' => '',
    ]);

return $sinopsis;


