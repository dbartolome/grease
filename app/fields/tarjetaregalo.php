<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$tarjetaregalo_page = get_page_by_path('tarjeta-regalo', 'ARRAY_N');

$tarjetaregalo = new FieldsBuilder('tarjetaregalo');

$tarjetaregalo
    ->setLocation('post_type', '==', 'page')
    ->and('post', '==', $tarjetaregalo_page[0]);

$tarjetaregalo
    ->addFields(get_field_partial('partials.fondo'))
    ->addTab('Imagen', ['placement' => 'left'])
        ->addImage('img_tarjetaregalo', [
            'label' => 'Imagen para la pagina de Tarjeta Regalo',
            'instructions' => 'Imagen para el contenido de Tarjeta Regalo',
            'required' => 0,
            'return_format' => 'array',
            'preview_size' => 'medium',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
        ])
    ->addTab('Importes', ['placement' => 'left'])
        ->addRepeater('importes', [
            'label' => 'Importes',
            'instructions' => 'Añade aquí las tarjetas regalo que quieras',
            'button_label' => 'Añadir importe',
        ])
            ->addText('importe', [
                'label' => 'Importe',
                'instructions' => 'Especifica el importe de esta tarjeta',
            ])
            ->addUrl('url_tarjeta', [
                'label' => 'URL Field',
                'instructions' => 'Especifica la URL de destino para este botón',
            ])
        ->endRepeater()
    ->addTab('Cómo canjear', ['placement' => 'left'])
        ->addRepeater('canjear', [
            'label' => 'Importes',
            'instructions' => 'Añade aquí los pasos que forman parte del proceso para canjear la tarjeta regalo.',
            'button_label' => 'Añadir paso',
        ])
            ->addText('paso', [
                'label' => 'Pregunta',
                'instructions' => 'Especifica una pregunta',
            ])
        ->endRepeater()
    ->addTab('Preguntas frecuentes', ['placement' => 'left'])
        ->addRepeater('preguntas', [
            'label' => 'Preguntas',
            'instructions' => 'Añade aquí las preguntas y respuestas frecuentes.',
            'button_label' => 'Añadir pregunta',
        ])
            ->addText('pregunta', [
                'label' => 'Pregunta',
                'instructions' => 'Especifica una pregunta',
            ])
            ->addTextarea('respuesta', [
                'label' => 'Respuesta',
                'instructions' => 'Especifica su respuesta',
            ])
        ->endRepeater()
;

return $tarjetaregalo;
