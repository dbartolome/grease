<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;


$generico = new FieldsBuilder('generico');

$generico
    ->setLocation('post_type', '==', 'page')
    ->and('page_template', '==', 'views/template-generico.blade.php');

$generico
    ->addFields(get_field_partial('partials.fondo'))

;

return $generico;
