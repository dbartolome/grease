<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

acf_add_options_page([
    'page_title' => get_bloginfo('name') . ' theme options',
    'menu_title' => 'Options',
    'menu_slug'  => 'theme-options',
    'capability' => 'edit_theme_options',
    'position'   => '999',
    'autoload'   => true
]);

$options = new FieldsBuilder('theme_options');

$options
    ->setLocation('options_page', '==', 'theme-options');

$options
    ->addTab('Portada', ['placement' => 'left'])
        ->addImage('background_img_home', [
            'label' => 'Imagen de fondo para portada',
            'instructions' => 'Tamaño requerido 000px x 000px',
            'required' => 0,
            'return_format' => 'array',
            'preview_size' => 'medium',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
        ])
        ->addRepeater('frames_foreground_img', [
            'label' => 'Imágenes de primer plano. Aprecerán en portada de forma animada. Habra una transición del tipo fade entre ellas"',
            'instructions' => 'Se requieren 4 imágenes de 958 x 1001 pixels ',
            'max' => 4,
            'min' => 4,
            'button_label' => 'Añadir un frame',
        ])
        ->addImage('foreground_img_home', [
            'label' => 'frame de primer plano para portada',
            'instructions' => 'Tamaño requerido 000px x 000px',
            'required' => 0,
            'return_format' => 'array',
            'preview_size' => 'thumbnail',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
        ])
        ->endRepeater()
    ->addImage('animacionFondo', [
        'label' => 'Imagen de fondo de la animacion',
        'instructions' => 'Tamaño requerido 000px x 000px',
        'required' => 0,
        'return_format' => 'array',
        'preview_size' => 'medium',
        'library' => 'all',
        'min_width' => '',
        'min_height' => '',
        'min_size' => '',
        'max_width' => '',
        'max_height' => '',
        'max_size' => '',
        'mime_types' => '',
    ])
    ->addTab('Footer', ['placement' => 'left'])
        ->addWysiwyg('texto_footer', [
            'label' => 'Texto para el footer',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'default_value' => '',
            'tabs' => 'all',
            'toolbar' => 'full',
            'media_upload' => 1,
            'delay' => 0,
        ])
        ->addImage('entradas_footer', [
            'label' => 'Entradas footer',
            'instructions' => 'Imagen para el botón entradas en el footer.',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'return_format' => 'array',
            'preview_size' => 'thumbnail',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
        ])
        ->addPageLink('entradas_link_footer', [
            'label' => 'Enlace botón entradas footer',
            'type' => 'page_link',
            'instructions' => 'Especificar la página de esta web a donde apunta el botón "entradas" del footer',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'post_type' => ['page'],
            'taxonomy' => [],
            'allow_null' => 0,
            'allow_archives' => 0,
            'multiple' => 0,
        ])
        ->addRepeater('social', [
            'label' => 'Iconos de redes sociales',
            'instructions' => 'Especifica aquí qué iconos de redes sociales se usarán y a qué destino apunta la URL de cada icono',
            'max' => 3,
            'min' => 0,
            'button_label' => 'Añadir un frame',
        ])
            ->addText('social_url', [
                'label' => 'URL',
                'instructions' => 'Especifica aquí la URL de la red social a la que apunta el enlace del icon',
                ])
            ->addText('social_icon', [
                'label' => 'Icono',
                'instructions' => 'Especifica aquí la referencia del icono (tal como aparece en Font Awesome)',
            ])
        ->endRepeater()
    ->addTab('Header', ['placement' => 'left'])
        ->addImage('entradas_header', [
            'label' => 'Entradas header',
            'instructions' => 'Imagen para el botón entradas en el header',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'return_format' => 'array',
            'preview_size' => 'thumbnail',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
        ])
        ->addPageLink('entradas_link_header', [
            'label' => 'Enlace botón entradas header',
            'type' => 'page_link',
            'instructions' => 'Especificar la página de esta web a donde apunta el botón "entradas" del header',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'post_type' => ['page'],
            'taxonomy' => [],
            'allow_null' => 0,
            'allow_archives' => 0,
            'multiple' => 0,
        ]);

;

return $options;
