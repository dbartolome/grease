<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$entradas_page = get_page_by_path('entradas', 'ARRAY_N');

$entradas = new FieldsBuilder('entradas');

$entradas
    ->setLocation('post_type', '==', 'page')
    ->and('post', '==', $entradas_page[0]);

$entradas
    ->addFields(get_field_partial('partials.fondo'))
    ->addTab('Calendario', ['placement' => 'left'])
    ->addWysiwyg('shortCode', [
        'label' => 'Short Code para el calendario',
        'instructions' => 'Aqui vamos a introducir el short code de los calendarios',
        'toolbar' => 'full',
        'media_upload' => 0,
    ])
    ->addTab('Venta de entradas', ['placement' => 'left'])
        ->addText('entradas_venta_titulo_sec', [
            'label' => 'Título de sección',
            'default_value' => 'Venta de entradas',
        ])
        ->addRepeater('entradas_venta_cards', [
            'label' => 'Tarjetas venta de entradas',
            'instructions' => 'Construye aquí las tarjetas que aparecen en esta sección. Todos los campos son opcionales',
            'layout' => 'row',
            'button_label' => 'Añadir tarjeta',
        ])
            ->addText('entradas_venta_titulo_card', [
                'label' => 'Título de tarjeta',
            ])
            ->addImage('entradas_venta_titulo_logo_card', [
                'label' => 'Logotipo para el título',
                'instructions' => 'Si se especifica una imagen aquí, el título de la tarjeta mostrará la imagen en lugar de un texto',
                'conditional_logic' => [],
                'wrapper' => [
                    'class' => 'logo-titulo',
                ],
                'return_format' => 'array',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])
            ->addFlexibleContent('entradas_venta_card_contents', [
                'instructions' => 'Contenido de tarjeta',
                'button_label' => 'Añadir elemento a esta tarjeta',
            ])
                ->addLayout('entradas_layout_enlace', [
                    'label' => 'Enlace',
                    'display' => 'block',
                ])
                    ->addRadio('entradas_layout_enlace_tipo', [
                        'label' => 'Tipo de enlace',
                        'choices' => ['img' => 'Imagen', 'boton' => 'Botón', 'normal' => 'Normal'],
                        'allow_null' => 0,
                        'layout' => 'horizontal',
                        'return_format' => 'value',
                    ])
                    ->addLink('entradas_layout_enlace_enlace', [
                        'label' => 'Enlace',
                    ])
                    ->addImage('entradas_layout_enlace_imagen', [
                        'label' => 'Imagen para el enlace',
                        'instructions' => 'Si se especifica una imagen aquí, el título de la tarjeta mostrará la imagen en lugar de un texto',
                        'required' => 1,
                        'wrapper' => [
                            'class' => 'logo-titulo',
                        ],
                        'return_format' => 'array',
                        'preview_size' => 'thumbnail',
                        'library' => 'all',
                    ])
                    ->conditional('entradas_layout_enlace_tipo', '==', 'img')
                ->addLayout('entradas_layout_texto', [
                    'label' => 'Texto',
                    'display' => 'block',
                ])
                    ->addWysiwyg('entradas_layout_texto_textarea', [
                        'label' => 'Texto',
                        'instructions' => 'Introduce aquí cualquier texto que deba aparecer en esta tarjeta',
                        'toolbar' => 'full',
                        'media_upload' => 0,
                    ])
        ->endRepeater()
        ->addTextarea('entradas_venta_footer', [
            'label' => 'Párrafo para el pie',
        ])
    ->addTab('Consulta precios y horarios', ['placement' => 'left'])
        ->addText('entradas_info_titulo_sec', [
            'label' => 'Título de sección',
            'default_value' => 'Consulta aquí precios y horarios',
        ])
        ->addRepeater('entradas_info_franjas_horarias', [
            'label' => 'Franjas horarias',
            'instructions' => 'Establece las franjas horarias con un mismo precio de entrada',
            'layout' => 'row',
            'button_label' => 'Añadir franja horaria',
        ])
            ->addText('entradas_info_franja_epigrafe', [
                'label' => 'Epígrafe de la franja horaria',
            ])
            ->addText('entradas_info_franja_precio_platea_pref', [
                'label' => 'Precio en platea preferente',
            ])
            ->addText('entradas_info_franja_precio_platea', [
                'label' => 'Precio en platea',
            ])
            ->addText('entradas_info_franja_precio_palcos', [
                'label' => 'Precio en palcos',
            ])
            ->addText('entradas_info_franja_precio_anfi1', [
                'label' => 'Precio en anfiteatro 1',
            ])
            ->addText('entradas_info_franja_precio_anfi2_A', [
                'label' => 'Precio en anfiteatro 2, filas 1 y 2',
            ])
            ->addText('entradas_info_franja_precio_anfi2_B', [
                'label' => 'Precio en anfiteatro 2, fila 3',
            ])
        ->endRepeater()
        ->addRepeater('entradas_info_especiales', [
            'label' => 'Información especial',
            'instructions' => 'Establece los bloques de información especial',
            'layout' => 'column',
            'button_label' => 'Añadir bloque de información especial',
        ])
            ->addText('entradas_info_especiales_titulo', [
                'label' => 'Título del bloque',
            ])
            ->addWysiwyg('entradas_info_especiales_tcontenido', [
                'label' => 'Contenido del bloque',
                'toolbar' => 'basic',
                'media_upload' => 0,
            ])
        ->endRepeater()
        ->addRepeater('entradas_info_notas', [
            'label' => 'Notas al pie',
            'instructions' => 'Notas al pie de la sección precios y horarios',
            'layout' => 'column',
            'button_label' => 'Añadir nota al pie',
        ])
            ->addWysiwyg('entradas_info_nota', [
                'label' => 'Contenido del bloque',
                'toolbar' => 'basic',
                'media_upload' => 0,
            ])
        ->endRepeater()
    ->addTab('Promociones', ['placement' => 'left'])
        ->addRepeater('entradas_promo_cards', [
            'label' => 'Tarjetas para promociones',
            'instructions' => 'Construye aquí las tarjetas que aparecen en esta sección. Todos los campos son opcionales',
            'layout' => 'row',
            'button_label' => 'Añadir tarjeta',
        ])
            ->addText('entradas_promo_titulo_card', [
                'label' => 'Título de tarjeta',
            ])
            ->addImage('entradas_promo_titulo_logo_card', [
                'label' => 'Logotipo para el título',
                'instructions' => 'Si se especifica una imagen aquí, el título de la tarjeta mostrará la imagen en lugar de un texto',
                'conditional_logic' => [],
                'wrapper' => [
                    'class' => 'logo-titulo',
                ],
                'return_format' => 'array',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])
            ->addFlexibleContent('entradas_promo_card_contents', [
                'instructions' => 'Contenido de tarjeta',
                'button_label' => 'Añadir elemento a esta tarjeta',
            ])
                ->addLayout('entradas_promo_layout_enlace', [
                    'label' => 'Enlace',
                    'display' => 'block',
                ])
                    ->addRadio('entradas_promo_layout_enlace_tipo', [
                        'label' => 'Tipo de enlace',
                        'choices' => ['img' => 'Imagen', 'boton' => 'Botón', 'normal' => 'Normal'],
                        'allow_null' => 0,
                        'layout' => 'horizontal',
                        'return_format' => 'value',
                    ])
                    ->addLink('entradas_promo_layout_enlace_enlace', [
                        'label' => 'Enlace',
                    ])
                    ->addImage('entradas_promo_layout_enlace_imagen', [
                        'label' => 'Imagen para el enlace',
                        'instructions' => 'Si se especifica una imagen aquí, el título de la tarjeta mostrará la imagen en lugar de un texto',
                        'required' => 1,
                        'wrapper' => [
                            'class' => 'logo-titulo',
                        ],
                        'return_format' => 'array',
                        'preview_size' => 'thumbnail',
                        'library' => 'all',
                    ])
                    ->conditional('entradas_promo_layout_enlace_tipo', '==', 'img')
                ->addLayout('entradas_promo_layout_texto', [
                    'label' => 'Texto',
                    'display' => 'block',
                ])
                    ->addWysiwyg('entradas_promo_layout_texto_textarea', [
                        'label' => 'Texto',
                        'instructions' => 'Introduce aquí cualquier texto que deba aparecer en esta tarjeta',
                        'toolbar' => 'full',
                        'media_upload' => 1,
                    ])
        ->endRepeater()
    ->addTab('Otros puntos de venta', ['placement' => 'left'])
        ->addRepeater('entradas_otros_puntos', [
            'label' => 'Puntos de venta alternativos',
            'instructions' => 'Establece los puntos de venta alternativos.',
            'layout' => 'row',
            'button_label' => 'Añadir punto de venta alternativo',
        ])
            ->addLink('entradas_otros_puntos_enlace', [
                'label' => 'Enlace',
            ])
            ->addImage('entradas_otros_puntos_img', [
                'label' => 'Logotipo para el enlace',
                'instructions' => 'Si se especifica una imagen aquí, el enlace mostrará la imagen en lugar de un texto',
                'return_format' => 'array',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])
    ;


return $entradas;
