<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$equipo_creativo_page = get_page_by_path('equipo-creativo', 'ARRAY_N');

$equipo_creativo = new FieldsBuilder('equipo-creativo');

$equipo_creativo
    ->setLocation('post_type', '==', 'page')
    ->and('post', '==', $equipo_creativo_page[0]);

$equipo_creativo
    ->addFields(get_field_partial('partials.fondo'))
;

return $equipo_creativo;

