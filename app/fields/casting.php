<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$casting_page = get_page_by_path('casting', 'ARRAY_N');

$casting = new FieldsBuilder('casting');

$casting
    ->setLocation('post_type', '==', 'page')
    ->and('post', '==', $casting_page[0]);

$casting
    ->addFields(get_field_partial('partials.fondo'))

;


return $casting;

