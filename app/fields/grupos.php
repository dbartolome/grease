<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$grupos_page = get_page_by_path('grupos', 'ARRAY_N');

$grupos = new FieldsBuilder('grupos');

$grupos
    ->setLocation('post_type', '==', 'page')
    ->and('post', '==', $grupos_page[0]);

$grupos
    ->addFields(get_field_partial('partials.fondo'))
    ->addTab('Bloques', ['placement' => 'left'])
        ->addRepeater('grupos_bloques', [
            'label' => 'Bloques encabezado/contenido',
            'button_label' => 'Añadir bloque',
        ])
            ->addTextarea('grupos_h2', [
                'label' => 'Encabezado del bloque',
            ])
            ->addColorPicker('grupos_color', [
                'label' => 'Color del encabezado',
                'default_value' => '#e3609e',
            ])
            ->addWysiwyg('grupos_contenido', [
                'label' => 'Contenido del bloque',
                'instructions' => '',
                'default_value' => '',
                'tabs' => 'all',
                'toolbar' => 'basic',
                'media_upload' => 0,
                'delay' => 0,
            ])
        ->endRepeater()
    ->addTab('Imagen', ['placement' => 'left'])
        ->addImage('grupos_img', [
            'label' => 'Imagen',
            'instructions' => 'Imagen que aparece en primer plano',
            'return_format' => 'array',
            'preview_size' => 'medium',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
        ])
;


return $grupos;

