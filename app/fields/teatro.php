<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$teatro_page = get_page_by_path('teatro', 'ARRAY_N');

$teatro = new FieldsBuilder('teatro');

$teatro
    ->setLocation('post_type', '==', 'page')
    ->and('post', '==', $teatro_page[0]);

$teatro
    ->addFields(get_field_partial('partials.fondo'))
        ->addImage('fondo_teatro_img', [
            'label' => 'Fondo superior',
            'instructions' => 'Imagen de fondo personalizado para la sección superior de esta página',
            'required' => 0,
            'return_format' => 'array',
            'preview_size' => 'medium',
        ])
        ->addTrueFalse('activar_bg_teatro_sec_sup', [
            'label' => 'Activar este fondo',
            'instructions' => 'El fondo elegido aquí sólo aparecerá si está activo. En caso contrario, aparecerá el fondo por defecto (que es el que se defina para portada en las opciones del tema).',
            'default_value' => 0,
            'ui' => 1,
        ])
    ->addTab('Información', ['placement' => 'left'])
        ->addText('teatro_dir', [
            'label' => 'Dirección',
        ])
        ->addText('teatro_poblacion', [
            'label' => 'Código postal y población',
        ])
        ->addUrl('enlace_mapa', [
            'label' => 'Enlace a mapa',
        ])
        ->addRepeater('teatro_telefonos', [
            'label' => 'Teléfonos de contacto',
            'button_label' => 'Añadir teléfono de contacto',
        ])
            ->addText('teatro_tel_label', [
                'label' => 'Etiqueta del teléfono',
            ])
            ->addText('teatro_tel_numero', [
                'label' => 'Número de teléfono',
            ])
        ->endRepeater()
        ->addRepeater('teatro_horarios', [
            'label' => 'Horarios de funciones',
            'button_label' => 'Añadir horario de funciones',
        ])
            ->addText('teatro_horario', [
                'label' => 'Horario',
            ])
        ->endRepeater()
        ->addRepeater('teatro_horarios_taquilla', [
            'label' => 'Horarios de taquilla',
            'button_label' => 'Añadir horario de taquilla',
        ])
            ->addText('teatro_horario_taquilla', [
                'label' => 'Horario',
            ])
        ->endRepeater()
    ->addTab('Cómo llegar', ['placement' => 'left'])
        ->addRepeater('comollegar', [
            'label' => 'Medios de transporte',
            'instructions' => 'Especifica aquí los medios de transporte para llegar al teatro',
            'button_label' => 'Añadir un medio de transprote',
        ])
            ->addText('comollegar_icon', [
                'label' => 'Icono',
                'instructions' => 'Especifica aquí la referencia del icono <a href="https://fontawesome.com/icons?d=gallery" taget="_blank">tal como aparece en Font Awesome</a>',
            ])
            ->addColorPicker('color_icon', [
                'label' => 'Color del icono',
                'default_value' => '#e3609e',
            ])
            ->addText('comollegar_label', [
                'label' => 'Nombre del medio',
                'instructions' => 'Escribe el nombre del medio de transporte',
            ])
            ->addText('comollegar_texto', [
                'label' => 'Indicaciones',
                'instructions' => 'Escribe las indicaciones necesarias para el medio de transporte',
            ])
        ->endRepeater();


return $teatro;

